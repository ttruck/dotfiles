## Programs on my System

**#1 [Kitty](https://github.com/kovidgoyal/kitty)**

A GPU accelerated terminal. Kitty comes with a good configuration by default but I have changed a couple of things in my [**Kitty Config.**](https://gitlab.com/ttruck/dotfiles/-/tree/master/.config/kitty)

- Changed the color scheme
- Removed audio bell

**#2 [i3](https://github.com/Airblader/i3)**

A tiling window manager. I use i3-gaps because it allows for the use of gaps in between windows. In my [**i3 Config**](https://gitlab.com/ttruck/dotfiles/-/tree/master/.config/i3) I have changed the following.

- Added gaps and borders on windows
- Added autostart programs that will run on startup
- Added keybinds to applications I use often
- Changed the default colors

**#3 [i3status](https://github.com/Airblader/i3)**

A status bar for i3. I have 5 built-in widgets and 1 optional widget in my [**i3status Config**](https://gitlab.com/ttruck/dotfiles/-/tree/master/.config/i3status) and they are the following.

- A Memory percentage
- A CPU percentage
- A Volume percentage
- A Date
- An optional Battery percentage

**#4 [Nitrogen](https://github.com/l3ib/nitrogen)**

A wallpaper browser and setter. I have not changed from the default config of nitrogen but I do have a [**Collection of Wallpapers.**](https://gitlab.com/ttruck/wallpapers)

**#5 [Firefox](https://www.mozilla.org/en-US/firefox/new/)**

A web browser not based on chromium. As I have not changed the deafult config of the browser, I have added extensions that are listed below.

- [**Dark Space Theme**](https://addons.mozilla.org/firefox/addon/nicothin-space/)

**#5 [PCManFM](https://github.com/lxde/pcmanfm)**

A graphical file manager for the LXDE. Because PCManFM is made for a desktop environment it uses [GTK Theming](https://gtk.org/)



